#include <stdint.h>
#include <Arduino.h>

// They are associated with pins 0..14 in that order
static const unsigned char ADDRESS_PINS[15] = {9, 8, 7, 6, 5, 4, 3, 2, 10, 11, 12, 13, 14, 15, 16};
static const unsigned char DATA_PINS[8] = {19, 18, 17, 28, 30, 32, 34, 36};
static const unsigned char CE = 22;
static const unsigned char OE = 24;
static const unsigned char WE = 26;
static const int DELAY_WRITE = 100; // us
static const int DELAY_READ = 100; // us
/*
 * Set the lo/hi bits of address pins seperately--this will help with page-aligned writes.
 */
void set_address_lo(uint16_t address) {
  for (int i = 0; i < 6; i++) digitalWrite(ADDRESS_PINS[i], ((address & (1 << i)) ?1:0));
}

void set_address_hi(uint16_t address) {
  for (int i = 6; i < 15; i++) digitalWrite(ADDRESS_PINS[i], ((address & (1 << i)) ?1:0));
}

void set_address(uint16_t address)
{
  set_address_lo(address);
  set_address_hi(address);
}


void config_data_pins(uint8_t direction) {
  for(int i = 0; i < 8; i++) pinMode(DATA_PINS[i], direction);
}

void set_data(uint8_t data)
{
  for (int i = 0; i < 8; i++) digitalWrite(DATA_PINS[i], ((data & (1 << i)) ?HIGH:LOW));
}

uint8_t read_data()
{
  uint8_t data = 0;
  for (int i = 0; i < 8; i++) data |= digitalRead(DATA_PINS[i]) << i;
  return data;
}

void read_enable()
{
  digitalWrite(CE, LOW);
  digitalWrite(OE, LOW);
  digitalWrite(WE, HIGH);
}
void write_enable()
{
  digitalWrite(CE, LOW);
  digitalWrite(OE, HIGH);
  digitalWrite(WE, LOW);
}
void standby()
{
  digitalWrite(CE, HIGH);
  digitalWrite(OE, HIGH);
  digitalWrite(WE, HIGH);
}

void write_byte(uint16_t address, uint8_t byte)
{
  set_address(address);
  set_data(byte);
  write_enable();
  delayMicroseconds(DELAY_WRITE);
  standby();
}

void write_page(uint16_t base_address, uint8_t (&page)[64])
{
  
  set_address_hi(base_address);
  for(int it=0; it<64; it++) {
    set_address_lo(it);
    set_data(page[it]);
    write_enable();
    delayMicroseconds(DELAY_WRITE);
    standby();
  }
}

void disable_SDP()
{
  write_byte(0x5555, 0xAA);
  write_byte(0x2AAA, 0x55);
  write_byte(0x5555, 0x80);
  write_byte(0x5555, 0xAA);
  write_byte(0x2AAA, 0x55);
  write_byte(0x5555, 0x10);
  write_byte(0x0000, 0x00);
  write_byte(0x0000, 0x00);
}
void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  while(!Serial);
  Serial.println("Hello World!");

  for(int i = 0; i < 15; i++) pinMode(ADDRESS_PINS[i], OUTPUT);
  pinMode(CE, OUTPUT);
  pinMode(OE, OUTPUT);
  pinMode(WE, OUTPUT);
  standby();
  delay(100);
}

bool gaurd = 0;
void loop() {
  if(!gaurd) {
    uint8_t page[64];
    memset(&page, 0, 64);
    page[0] = 0xDE;
    page[2] = 0xAD;
    config_data_pins(OUTPUT);
    write_page(0xFF00, page);

    
    delay(100);
    config_data_pins(INPUT);
    set_address(0xFF02);
    read_enable();
    delay(20);
    Serial.println(read_data());
    standby();
  }
  gaurd = 1;
  while(1);
}